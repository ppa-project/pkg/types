// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package types

import (
	"encoding/json"
	"fmt"
)

// The DatabaseAttribute type defines a column in the database.
type DatabaseAttribute struct {
	// The name of the attribute as it occurs in the database
	Name string

	// The type; can be "int", "string", "enum"
	Kind string

	// In case of kind=="enum": a list of possible values
	// In all other cases, a nil slice
	EnumValues []string
}

func (dba *DatabaseAttribute) Equal(other *DatabaseAttribute) bool {
	if dba.Name != other.Name || dba.Kind != other.Kind || len(dba.EnumValues) != len(other.EnumValues) {
		return false
	}
	for i := range dba.EnumValues {
		if dba.EnumValues[i] != other.EnumValues[i] {
			return false
		}
	}
	return true
}

// FilterDiscriminates checks whether a given filter is actually able to
// filter any values. For enum-type attributes, the filter is applied to
// each variant, and if all nor no variants match the filter, FilterDiscriminates
// returns false. For string-type attributes, false is returned only if the
// filter's reference value is empty. In all other cases, FilterDiscriminates
// returns true.
func (dba *DatabaseAttribute) FilterDiscriminates(filter *Filter) bool {
	switch dba.Kind {
	case "enum":
		matches := 0
		for i := range dba.EnumValues {
			if b, _ := filter.EvalString(dba.EnumValues[i]); b {
				matches++
			}
		}
		return matches > 0 && matches < len(dba.EnumValues)
	case "string":
		return filter.ReferenceValue != ""
	default:
		return true
	}
}

type DatabaseAttributes []DatabaseAttribute

func (dba *DatabaseAttributes) Equal(other *DatabaseAttributes) bool {
	if len(*dba) != len(*other) {
		return false
	}

	m := make(map[string]*DatabaseAttribute)
	for i := range *dba {
		m[(*dba)[i].Name] = &(*dba)[i]
	}

	for i := range *other {
		if v, ok := m[(*other)[i].Name]; !ok || !v.Equal(&(*other)[i]) {
			return false
		}
	}
	return true
}

func (dba *DatabaseAttributes) MarshalJSON() ([]byte, error) {
	slice := []DatabaseAttribute(*dba)
	mapping := make(map[string]interface{})

	for i := range slice {
		if slice[i].Kind == "enum" {
			mapping[slice[i].Name] = slice[i].EnumValues
		} else {
			mapping[slice[i].Name] = slice[i].Kind
		}
	}

	return json.Marshal(mapping)
}

func (dba *DatabaseAttributes) UnmarshalJSON(bytes []byte) error {
	var mapping map[string]interface{}
	if err := json.Unmarshal(bytes, &mapping); err != nil {
		return err
	}

	slice := make([]DatabaseAttribute, 0)

	for k := range mapping {
		switch value := mapping[k].(type) {
		case string:
			if value == "int" || value == "string" {
				slice = append(slice, DatabaseAttribute{
					Name: k,
					Kind: value,
				})
			} else {
				return fmt.Errorf(`Attribute description for attribute %s must be "int" or "string" or a slice but is instead %s`, k, value)
			}
		case []interface{}:
			enumValues := make([]string, len(value))
			for i := range value {
				var ok bool
				if enumValues[i], ok = value[i].(string); !ok {
					return fmt.Errorf("Element %d (%v) of value for attribute %s is not a string but is instead %T", i, value[i], k, value[i])
				}
			}
			slice = append(slice, DatabaseAttribute{
				Name:       k,
				Kind:       "enum",
				EnumValues: enumValues,
			})
		default:
			return fmt.Errorf("Attribute description for attribute %s (%v) is not string or []string but is instead %T", k, value, value)
		}
	}

	*dba = DatabaseAttributes(slice)
	return nil
}
