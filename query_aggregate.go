// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package types

import (
	"encoding/json"
	"fmt"
	"regexp"

	"github.com/rs/zerolog/log"
)

// Aggregate specifies the aggregate to be computed.
type Aggregate struct {
	// Function to be calculated over the attributes, such as the sum,
	// average or count of the attributes.
	Function AggregateFunction

	// The owner party of the attribute (as identified by their short name).
	// Empty in case of COUNT.
	Owner string

	// The name of the attribute in the owner's database. Empty in case
	// of COUNT.
	Attribute string
}

// AggregateFunction is the type of function supported by our MPC protocol.
type AggregateFunction string

// These are the only valid values for an AggregateFunction.
const (
	CountFunction      AggregateFunction = "COUNT"
	SumFunction                          = "SUM"
	MeanFunction                         = "MEAN"
	MeanStdDevFunction                   = "MEANSTDEV"
)

// String returns the string representation of Aggregate.
func (agg *Aggregate) String() string {
	if agg.Owner == "" && agg.Attribute == "" {
		// It's probably COUNT?
		return string(agg.Function)
	}
	return string(agg.Function) + "(" + agg.Owner + ":" + agg.Attribute + ")"
}

// MarshalJSON returns the JSON representation for Aggregate.
// This is currently a quoted version of qc.String().
func (agg *Aggregate) MarshalJSON() ([]byte, error) {
	return []byte("\"" + agg.String() + "\""), nil
}

// UnmarshalJSON tries to parse the data into a Aggregate.
// It expects data of the form "FUNCTION(owner.attribute)".
func (agg *Aggregate) UnmarshalJSON(data []byte) error {
	var str string
	err := json.Unmarshal(data, &str)
	if err != nil {
		log.Debug().Err(err).Str("data", string(data)).Msg("types: Failed to unmarshal query aggregate")
		str = string(data)
	}
	var qcRegexp = regexp.MustCompile(`^((COUNT(?:\(\))?)|((SUM|MEAN|MEANSTDEV)(\((\w+):([\w-]+(?: [\w-]+)*)\))))$`)
	matches := qcRegexp.FindStringSubmatch(str)
	if matches == nil || len(matches) != 8 {
		return fmt.Errorf("types: Unable to parse query aggregate: %s", str)
	}
	if matches[1] == "COUNT" || matches[1] == "COUNT()" {
		agg.Function = CountFunction
	} else {
		agg.Function = AggregateFunction(matches[4])
	}
	agg.Owner = matches[6]
	agg.Attribute = matches[7]
	return nil
}
