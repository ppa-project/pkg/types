// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package types contains type definitions shared by the different packages in the PPA project.
package types

// Query is the type of query this MPC system supports
type Query struct {
	Aggregates []Aggregate `json:"aggregates" binding:"required" swaggertype:"array,string" example:"party2:attr2 == 1"`
	Filters    []Filter    `json:"filters" binding:"required" swaggertype:"array,string" example:"SUM(party1:attr1)"`
}

func (q *Query) String() string {
	// There is at least one aggregate and one filter in a correct query
	if len(q.Aggregates) == 0 || len(q.Filters) == 0 {
		return "Invalid query"
	}

	s := "SELECT " + q.Aggregates[0].String()
	for i := 1; i != len(q.Aggregates); i++ {
		s += ", " + q.Aggregates[i].String()
	}

	s += " FOR " + q.Filters[0].String()
	for i := 1; i != len(q.Filters); i++ {
		s += ", " + q.Filters[i].String()
	}

	return s
}

// After being approved by the coordinator, a query is assigned an ID.
// A tagged query is a query and its corresponding ID.
type TaggedQuery struct {
	ID string `example:"86958258745911c77fbcf1519c6fb2dec4691c3b92bb9e71df696b6d5a8ab1cd"`
	*Query
}
