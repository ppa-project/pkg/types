// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package types

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFilter(t *testing.T) {
	assert := assert.New(t)
	fil := &Filter{}
	inputFil := "\"TNO:rockets > 5\"\n"
	err := json.Unmarshal([]byte(inputFil), fil)
	assert.Nil(err, "No error expected")
	assert.Equal("TNO", fil.Owner, "Unexpected attribute owner")
	assert.Equal("rockets", fil.Attribute, "Unexpected attribute name")
	assert.Equal(">", fil.Operator, "Unexpected operator")
	assert.Equal("5", fil.ReferenceValue, "Unexpected attribute value")

	// Now the other way around
	// Oh, and we're doing it this way because otherwise "<" gets turned into \\u003e
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(false)
	err = enc.Encode(fil)
	assert.Nil(err, "No error expected")
	assert.Equal(inputFil, string(buf.String()), "Results should be the same after unmarshal + marshal ")
}

func TestFilter2(t *testing.T) {
	assert := assert.New(t)
	fil := &Filter{}
	inputFil := "\"PartyA:Kosten 2016 == 123\"\n"
	err := json.Unmarshal([]byte(inputFil), fil)
	assert.Nil(err, "No error expected")
	assert.Equal("PartyA", fil.Owner, "Unexpected attribute owner")
	assert.Equal("Kosten 2016", fil.Attribute, "Unexpected attribute name")
	assert.Equal("==", fil.Operator, "Unexpected operator")
	assert.Equal("123", fil.ReferenceValue, "Unexpected attribute value")
}

func TestFilterValids(t *testing.T) {
	filters := []string{
		"tno:rockets > 5",
		"TNO:ROCKETS > 5",
		"TNO1:rockets > 5",
		"TNO:rockets1 > 5",
		"TNO:rockets > Abc1",

		"TNO:rockets == 5",
		"TNO:rockets != 5",
		"TNO:rockets < 5",
		"TNO:rockets > 5",
		"TNO:rockets <= 5",
		"TNO:rockets >= 5",

		"PartyB:Startdatum > 2020-12-23",
		"PartyB:Startdatum < 2020-12-23",
		"PartyB:Startdatum >= 2001-12-23",
		"PartyB:Startdatum <= 2004-12-23",
		"PartyB:Startdatum == 2020-11-23",
		"PartyB:Startdatum != 2020-9-23",
		"PartyB:Startdatum != 2020-09-23",

		"PartyB:Gebruik == Ja",
		"PartyA:Kosten 2016 == 123",
	}
	for _, f := range filters {
		fil := &Filter{}
		err := json.Unmarshal([]byte("\""+f+"\"\n"), fil)
		assert.NoError(t, err, f)
		assert.NotEmpty(t, fil.Owner)
		assert.NotEmpty(t, fil.Attribute)
		assert.NotEmpty(t, fil.Operator)
		assert.NotEmpty(t, fil.ReferenceValue)
	}
}

func TestFilterInvalids(t *testing.T) {
	filters := []string{
		":rockets > 5",
		"TNO > 5",
		"TNO: > 5",
		"TNO:rockets 5",
		"TNO:rockets >",
		"TNO:rockets !! 5",
		"TNO:rockets >5",
		"TNO:rockets> 5",
		"TNO:rockets>5",
		"TNO: rockets > 5",
		"TNO :rockets > 5",
		" TNO:rockets > 5 ",
		"PartyB:Startdatum == 2020.11.23",
		"PartyB:Startdatum != 2020 9 23",
	}
	for _, f := range filters {
		fil := &Filter{}
		err := json.Unmarshal([]byte("\""+f+"\"\n"), fil)
		assert.Error(t, err, f)
	}
}

func TestAggregate1(t *testing.T) {
	assert := assert.New(t)
	qf := &Aggregate{}
	inputQf := "\"SUM(TNO:cars)\""
	err := json.Unmarshal([]byte(inputQf), qf)
	assert.Nil(err, "No error expected")
	assert.Equal(AggregateFunction("SUM"), qf.Function, "Unexpected function")
	assert.Equal("TNO", qf.Owner, "Unexpected attribute owner")
	assert.Equal("cars", qf.Attribute, "Unexpected attribute")

	// Now the other way around
	qfT, err := json.Marshal(qf)
	assert.Nil(err, "No error expected")
	assert.Equal(inputQf, string(qfT), "Results should be the same after unmarshal + marshal")
}

func TestAggregate2(t *testing.T) {
	assert := assert.New(t)
	qf := &Aggregate{}
	inputQf := "\"COUNT\""
	err := json.Unmarshal([]byte(inputQf), qf)
	assert.Nil(err, "No error expected")
	assert.Equal(AggregateFunction("COUNT"), qf.Function, "Unexpected function")
	assert.Equal("", qf.Owner, "Attribute owner should be empty for COUNT")
	assert.Equal("", qf.Attribute, "Attribute should be empty for COUNT")

	// Now the other way around
	qfT, err := json.Marshal(qf)
	assert.Nil(err, "No error expected")
	assert.Equal(inputQf, string(qfT), "Results should be the same after unmarshal + marshal")
}

func TestAggregate3(t *testing.T) {
	assert := assert.New(t)
	qf := &Aggregate{}
	inputQf := "\"MEAN(PartyA:Kosten 2016)\""
	err := json.Unmarshal([]byte(inputQf), qf)
	assert.Nil(err, "No error expected")
	assert.Equal(AggregateFunction("MEAN"), qf.Function, "Unexpected function")
	assert.Equal("PartyA", qf.Owner, "Unexpected attribute owner")
	assert.Equal("Kosten 2016", qf.Attribute, "Unexpected attribute")

	// Now the other way around
	qfT, err := json.Marshal(qf)
	assert.Nil(err, "No error expected")
	assert.Equal(inputQf, string(qfT), "Results should be the same after unmarshal + marshal")
}

func TestAggragateValids(t *testing.T) {
	aggregates := []string{
		"COUNT",
		"COUNT()",
		"SUM(TNO:cars)",
		"MEAN(TNO:cars)",
		"MEANSTDEV(TNO:cars)",
		"SUM(TNO1:cars)",
		"SUM(TNO:cars1)",
		"SUM(1:cars)",
		"SUM(TNO:1)",
		"MEAN(PartyA:Kosten 2016)",
	}
	for _, a := range aggregates {
		agg := &Aggregate{}
		err := json.Unmarshal([]byte("\""+a+"\"\n"), agg)
		assert.NoError(t, err, a)
	}
}

func TestAggragateInvalids(t *testing.T) {
	aggregates := []string{
		"SUM",
		"MEAN",
		"MEANSTDEV",
		"SUM()",
		"()",
		"SUM(TNO)",
		"SUM(TNO:)",
		"SUM(:cars)",
		"(TNO:cars)",
		" (TNO:cars)",
		"sum(TNO:cars)",
		"SUM(TNO:cars",
		"SUM TNO:cars",
		"SUM TNO:cars)",
		"RANDOM(TNO:cars)",
		"SUM (TNO:cars)",
		"SUM( TNO:cars)",
		"SUM(TNO :cars)",
		"SUM(TNO: cars)",
		"SUM(TNO:cars )",
		"COUNT(TNO1:cars)",
		"COUNT(TNO:cars1)",
		"COUNT(1:cars)",
		"COUNT(TNO:1)",
	}
	for _, a := range aggregates {
		agg := &Aggregate{}
		err := json.Unmarshal([]byte("\""+a+"\"\n"), agg)
		assert.Error(t, err, a)
	}
}

func TestQueryString(t *testing.T) {
	assert := assert.New(t)

	assert.Equal("SELECT SUM(a:a1) FOR b:b1 == 2", (&Query{
		Aggregates: []Aggregate{{Function: SumFunction, Owner: "a", Attribute: "a1"}},
		Filters:    []Filter{{Owner: "b", Attribute: "b1", Operator: "==", ReferenceValue: "2"}},
	}).String())

	assert.Equal("SELECT MEAN(a:a1) FOR b:b1 == 2, c:c1 > j", (&Query{
		Aggregates: []Aggregate{{Function: MeanFunction, Owner: "a", Attribute: "a1"}},
		Filters: []Filter{
			{Owner: "b", Attribute: "b1", Operator: "==", ReferenceValue: "2"},
			{Owner: "c", Attribute: "c1", Operator: ">", ReferenceValue: "j"},
		},
	}).String())

	assert.Equal("SELECT COUNT, MEANSTDEV(a:a1) FOR c:c1 <= j", (&Query{
		Aggregates: []Aggregate{
			{Function: CountFunction, Owner: "", Attribute: ""},
			{Function: MeanStdDevFunction, Owner: "a", Attribute: "a1"},
		},
		Filters: []Filter{{Owner: "c", Attribute: "c1", Operator: "<=", ReferenceValue: "j"}},
	}).String())

	assert.Equal("Invalid query", (&Query{
		Filters: []Filter{{Owner: "c", Attribute: "c1", Operator: "<=", ReferenceValue: "j"}},
	}).String())

	assert.Equal("Invalid query", (&Query{
		Aggregates: []Aggregate{{Function: SumFunction, Owner: "a", Attribute: "a1"}},
	}).String())

	assert.Equal("Invalid query", (&Query{}).String())
}
