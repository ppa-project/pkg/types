// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package types

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMarshalDatabaseAttributes(t *testing.T) {
	assert := assert.New(t)

	dba := DatabaseAttributes([]DatabaseAttribute{
		{Name: "A", Kind: "int"},
		{Name: "B", Kind: "string"},
		{Name: "C", Kind: "enum", EnumValues: []string{"d", "e"}},
	})

	bytes, err := json.Marshal(&dba)
	assert.Nil(err)
	assert.Equal(`{"A":"int","B":"string","C":["d","e"]}`, string(bytes))
}

func TestUnmarshalDatabaseAttributes(t *testing.T) {
	assert := assert.New(t)

	goodBytes := []byte(`{"A":"int","B":"string","C":["d","e"]}`)
	var dba DatabaseAttributes
	err := json.Unmarshal(goodBytes, &dba)
	assert.Nil(err)

	assert.ElementsMatch(DatabaseAttributes([]DatabaseAttribute{
		{Name: "A", Kind: "int"},
		{Name: "B", Kind: "string"},
		{Name: "C", Kind: "enum", EnumValues: []string{"d", "e"}},
	}), dba)

	testErrorUnmarshalDatabaseAttributes(t,
		[]byte(`{"A":"fred"}`),
		`Attribute description for attribute A must be "int" or "string" or a slice but is instead fred`,
	)

	testErrorUnmarshalDatabaseAttributes(t,
		[]byte(`{"A":["u",3,"v"]}`),
		"Element 1 (3) of value for attribute A is not a string but is instead float64",
	)

	testErrorUnmarshalDatabaseAttributes(t,
		[]byte(`{"A":7}`),
		`Attribute description for attribute A (7) is not string or []string but is instead float64`,
	)
}

func testErrorUnmarshalDatabaseAttributes(t *testing.T, bytes []byte, expected string) {
	var dba DatabaseAttributes
	err := json.Unmarshal(bytes, &dba)
	assert.Equal(t, expected, err.Error())
}

func TestDatabaseAttributesEqual(t *testing.T) {
	dba1 := DatabaseAttributes([]DatabaseAttribute{
		{Name: "A", Kind: "int"},
		{Name: "B", Kind: "string"},
		{Name: "C", Kind: "enum", EnumValues: []string{"d", "e"}},
	})

	dba2 := DatabaseAttributes([]DatabaseAttribute{
		{Name: "C", Kind: "enum", EnumValues: []string{"d", "e"}},
		{Name: "B", Kind: "string"},
		{Name: "A", Kind: "int"},
	})

	assert.True(t, (&dba1).Equal(&dba1))
	assert.True(t, (&dba1).Equal(&dba2))
	assert.True(t, (&dba2).Equal(&dba1))
}

func TestDatabaseAttributesNotEqual(t *testing.T) {
	dbas := []DatabaseAttributes{
		DatabaseAttributes([]DatabaseAttribute{
			{Name: "A", Kind: "int"},
			{Name: "B", Kind: "string"},
			{Name: "C", Kind: "enum", EnumValues: []string{"d", "e"}},
		}),
		DatabaseAttributes([]DatabaseAttribute{
			{Name: "A", Kind: "int"},
			{Name: "B", Kind: "int"},
			{Name: "C", Kind: "enum", EnumValues: []string{"d", "e"}},
		}),
		DatabaseAttributes([]DatabaseAttribute{
			{Name: "A", Kind: "int"},
			{Name: "B", Kind: "string"},
			{Name: "C", Kind: "enum", EnumValues: []string{"d", "f"}},
		}),
		DatabaseAttributes([]DatabaseAttribute{
			{Name: "D", Kind: "int"},
			{Name: "B", Kind: "string"},
			{Name: "C", Kind: "enum", EnumValues: []string{"d", "e"}},
		}),
		DatabaseAttributes([]DatabaseAttribute{
			{Name: "F", Kind: "int"},
			{Name: "T", Kind: "int"},
			{Name: "E", Kind: "int"},
		}),
		DatabaseAttributes([]DatabaseAttribute{
			{Name: "A", Kind: "int"},
			{Name: "B", Kind: "string"},
		}),
		DatabaseAttributes([]DatabaseAttribute{
			{Name: "A", Kind: "int"},
			{Name: "B", Kind: "int"},
		}),
	}

	for i := 0; i < len(dbas); i++ {
		for j := i + 1; j < len(dbas); j++ {
			assert.False(t, (&dbas[i]).Equal(&dbas[j]))
		}
	}
}

func TestDatabaseAttributeFilterDiscriminates(t *testing.T) {
	dba := DatabaseAttribute{Kind: "enum", EnumValues: []string{"m", "v", "x"}}

	assert.True(t, dba.FilterDiscriminates(&Filter{Operator: "==", ReferenceValue: "m"}))
	assert.True(t, dba.FilterDiscriminates(&Filter{Operator: ">=", ReferenceValue: "n"}))
	assert.True(t, dba.FilterDiscriminates(&Filter{Operator: ">", ReferenceValue: "m"}))
	assert.True(t, dba.FilterDiscriminates(&Filter{Operator: "!=", ReferenceValue: "v"}))
	assert.False(t, dba.FilterDiscriminates(&Filter{Operator: "==", ReferenceValue: "n"}))
	assert.False(t, dba.FilterDiscriminates(&Filter{Operator: ">=", ReferenceValue: "m"}))
	assert.False(t, dba.FilterDiscriminates(&Filter{Operator: ">", ReferenceValue: "z"}))
	assert.False(t, dba.FilterDiscriminates(&Filter{Operator: "!=", ReferenceValue: "c"}))

	dba = DatabaseAttribute{Kind: "string"}

	assert.True(t, dba.FilterDiscriminates(&Filter{Operator: "==", ReferenceValue: "m"}))
	assert.True(t, dba.FilterDiscriminates(&Filter{Operator: ">=", ReferenceValue: "n"}))
	assert.True(t, dba.FilterDiscriminates(&Filter{Operator: ">", ReferenceValue: "m"}))
	assert.True(t, dba.FilterDiscriminates(&Filter{Operator: "!=", ReferenceValue: "v"}))
	assert.False(t, dba.FilterDiscriminates(&Filter{Operator: "==", ReferenceValue: ""}))
	assert.False(t, dba.FilterDiscriminates(&Filter{Operator: ">=", ReferenceValue: ""}))
	assert.False(t, dba.FilterDiscriminates(&Filter{Operator: ">", ReferenceValue: ""}))
	assert.False(t, dba.FilterDiscriminates(&Filter{Operator: "!=", ReferenceValue: ""}))
}
