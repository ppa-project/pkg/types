// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package types

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"

	"github.com/rs/zerolog/log"
)

var ErrorUnknownOperator = errors.New("Unknown operator in filter")

// Filter specifies a filter on a specific attribute.
// Filters compare an attribute against a reference value, defining a rule of
// the form "Contoso:Age > 37".
type Filter struct {
	// The owner party of the attribute (as identified by their short name)
	Owner string

	// The name of the attribute in the owner's database
	Attribute string

	// The operator to be used; should be one of ==, !=, >, <, >= and <=.
	Operator string

	// The reference value to compare against. If the attribute is an integer
	// type, this value will be parsed as an integer.
	ReferenceValue string
}

// String returns the string representation of a Filter
func (filter *Filter) String() string {
	return filter.Owner + ":" + filter.Attribute + " " + filter.Operator + " " + filter.ReferenceValue
}

// MarshalJSON returns the JSON representation of Filter
// This is currently a quoted version of filter.String().
func (filter *Filter) MarshalJSON() ([]byte, error) {
	return []byte("\"" + filter.String() + "\""), nil
}

// UnmarshalJSON tries to parse the data into a Filter
// It expects data of the form "owner:attribute==value",
// allowing any comparison in place of ==.
func (filter *Filter) UnmarshalJSON(data []byte) error {
	var str string
	err := json.Unmarshal(data, &str)
	if err != nil {
		log.Debug().Err(err).Str("data", string(data)).Msg("types: Failed to unmarshal query filter")
		str = string(data)
	}
	var filterRegexp = regexp.MustCompile(`^(\w+)\:([\w-]+(?: [\w-]+)*) (==|!=|>|<|>=|<=) ((?:\w|-)+)$`)
	matches := filterRegexp.FindStringSubmatch(str)
	if matches == nil || len(matches) != 5 {
		return fmt.Errorf("types: Unable to parse query filter: %s", str)
	}
	filter.Owner = matches[1]
	filter.Attribute = matches[2]
	filter.Operator = matches[3]
	filter.ReferenceValue = matches[4]
	return nil
}

// FilterFromString is a convenience function that returns a Filter from
// a string description
func NewFilterFromString(input string) *Filter {
	filter := &Filter{}
	filter.UnmarshalJSON([]byte("\"" + input + "\""))
	return filter
}

// EvalInt evaluates the condition provided a value.
//
// Query filters are conditions of the form "owner.attribute operator reference-value".
// To evaluate a value given a filter, the value v is inserted at "owner.attribute".
// For example: given a filter "A.B == 7" and a v of 42, EvalInt returns the value
// of "42 == 7" (false).
func (filter *Filter) EvalInt(v int64) (bool, error) {
	// TODO: we could see if caching this value is more efficient
	intReferenceValue, err := strconv.ParseInt(filter.ReferenceValue, 10, 64)
	if err != nil {
		return false, err
	}
	switch filter.Operator {
	case "==":
		return v == intReferenceValue, nil
	case "!=":
		return v != intReferenceValue, nil
	case "<":
		return v < intReferenceValue, nil
	case ">":
		return v > intReferenceValue, nil
	case "<=":
		return v <= intReferenceValue, nil
	case ">=":
		return v >= intReferenceValue, nil
	}
	return false, ErrorUnknownOperator
}

// EvalString evaluates the condition provided a value.
//
// It operates in the same way as EvalInt, but performs a string comparison instead.
func (filter *Filter) EvalString(v string) (bool, error) {
	switch filter.Operator {
	case "==":
		return v == filter.ReferenceValue, nil
	case "!=":
		return v != filter.ReferenceValue, nil
	case "<":
		return v < filter.ReferenceValue, nil
	case ">":
		return v > filter.ReferenceValue, nil
	case "<=":
		return v <= filter.ReferenceValue, nil
	case ">=":
		return v >= filter.ReferenceValue, nil
	}
	return false, ErrorUnknownOperator
}
